/**
 *  Tong Wen
 *  Update: 2014/07/11
 */


Summary
-------
This is a wrapper function of Tonwen Project for Web, and provide a block
which has two link to switch between Traditional Chinese and Simplify Chinese,
using on-page translation by javascript provided by Tonwen Project:
http://tongwen.openfoundry.org/

This module also include jquery.cookie library and Tongwen for Web library.


Basic Functionality
-------------------
 * Provide a block to switch between Traditional Chinese and Simplify Chinese
 * Remember setting which has be switched (in the cookies)


Installation and Settings
-------------------------

1. Enable this module (/admin/build/modules)
2. Enable "TongWen" block (/admin/build/block)


Author & Maintainers
------
Jimmy Huang (jimmy@jimmyhub.net, http://drupaltaiwan.org, user name jimmy)
Albert Liu (https://www.drupal.org/u/albert.liu)

If you use this module, find it useful, and want to send the author a thank
or you note, feel free to contact me.

The author can also be contacted for paid customizations of this and other modules.